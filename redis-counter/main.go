package main

import (
	"context"
	"fmt"
	"net/http"
	"strconv"

	"github.com/go-redis/redis/v8"
	"github.com/labstack/echo/v4"
)

func main() {
	ctx := context.Background()
	e := echo.New()
	rdb := redis.NewClient(&redis.Options{
		Addr: "redis-server:6379",
	})

	err := rdb.Set(ctx, "visits", 0, 0).Err()
	if err != nil {
		panic(err)
	}
	e.GET("/", func(c echo.Context) error {
		ctx := c.Request().Context()
		visits, err := rdb.Get(ctx, "visits").Result()
		if err != nil {
			return c.JSON(http.StatusInternalServerError, fmt.Sprintf("error getting data %v", err))
		}
		visitsNumber, err := strconv.Atoi(visits)
		if err != nil {
			return c.JSON(http.StatusInternalServerError, fmt.Sprintf("error parsing data %v", err))
		}
		err = rdb.Set(ctx, "visits", visitsNumber+1, 0).Err()
		if err != nil {
			return c.JSON(http.StatusInternalServerError, fmt.Sprintf("error setting data %v", err))
		}
		return c.String(http.StatusOK, fmt.Sprintf("Number of visits so far %s", visits))
	})
	e.Logger.Fatal(e.Start(":8081"))
}
